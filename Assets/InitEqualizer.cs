﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitEqualizer : MonoBehaviour {
	public GameObject baseCubePrefab;
	GameObject[] equalizerCubes = new GameObject[512];
	public float maxScale;
	// Use this for initialization
	void Start () {
        // TODO #3 instanziieren neuer Cubes, pro Frequenz ein Cube
        // baseCubePrefab kann in der Unity Oberfläche mit dem Prefab aus dem Packgage gefüllt werden (drag and drop)
        // equalizerCubes[] sollte später alle Cubes beinhalten
        for (int i = 0; i < 512; i++)
		{
			GameObject actualCube = (GameObject)Instantiate (baseCubePrefab);
            actualCube.transform.position = this.transform.position;  // Position vom "Elternelement"
            actualCube.transform.parent = this.transform;
			actualCube.name = "EqualizerCube"+i;
			this.transform.eulerAngles = new Vector3 (0, -0.703125f * i, 0);  // drehen des "Parent objectes"
			actualCube.transform.position = Vector3.forward * 100;
			equalizerCubes[i] = actualCube;  // Würfel zum Array hinzufügen
		}
	}
	
	// Update is called once per frame
	void Update () {
		// TODO #3 die Skalierung jedes Cubes soll entsprechend der Freuqnzvorkommen angepasst werden
		// Prüfen auf "cube != null" evtl. sinnvoll
		// maxScale kann in der Oberfläche eingestellt werden um die Darstellung zu verbessern
		for (int i = 0; i < 512; i++)
		{
			if (baseCubePrefab != null) {
				equalizerCubes[i].transform.localScale = new Vector3(2, AudioPeer.samples[i] * maxScale + 2, 2);
			}
		}
	}
}
