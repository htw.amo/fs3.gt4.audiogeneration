﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBands : MonoBehaviour {

	public int freqBandNo;  // TODO #4.1 in der Unity Oberfläche jedem Cube eine nummer geben 0 bis 7
	public float startScale;
	public float scaleMultiplier;

	// Use this for initialization
	void Start () {
		// Hier ist nichts zu tun
	}
	
	// Update is called once per frame
	void Update () {
		// TODO #4.1 bei jedem Update den Cube neu skalieren mit transform.localScale
		transform.localScale = new Vector3(transform.localScale.x, (AudioPeer.freqencyBands[freqBandNo] * scaleMultiplier) + startScale, transform.localScale.z);
	}
}
