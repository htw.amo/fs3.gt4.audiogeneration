﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]
public class AudioPeer : MonoBehaviour {

	AudioSource audioSource;
	public static float[] samples = new float[512];  // Aufteilen der Frequenzen in 512 Bereiche 
	public static float[] freqencyBands = new float[8];
 
	void Start () {
		audioSource = GetComponent<AudioSource> ();  // Legt das erste gefundene Gameobjekt vom Typ "Audisource" in die Variable
	}
	
	void Update () {
		GetAudioSpectrumFromAudiSource();
		MakeFrequencyBands();
	}

	void GetAudioSpectrumFromAudiSource() {
		// TODO #2 Spektrum Daten in das samples array schreiben
		audioSource.GetSpectrumData(samples, 0, FFTWindow.Hamming);  // nutzen der Fourier-Transformation
    }

    void MakeFrequencyBands()
    {
        /*

			TODO #4
			Aufteilen der 512 Samples in eine "sinnvolle" Anzahl von Frequenzbereichen
			22050 Frequenzen --> in 512 Samples --> 43Hz in einem Sample

			Die Frequenzkategiereien aus dem Vortrag sind:

			20	 - 60
			60	 - 250
			250	 - 500
			500	 - 2000
			2000 - 4000
			4000 - 6000
			6000 - 20000
			--> nur 7 Kategirien

			--> Neue Aufteilung

			i	Samples		Hz		Bereich
			0	2 			86		0		bis	86Hz
			1	4			172		87		bis	258
			2	8			344		259		bis 602
			3	16			688		603		bis 1290
			4	32			1376	1291	bis 2666
			5	64			2752	2667	bis 5418
			6	128			5504	5419	bis 10922
			7	256			11008	10923	bis 21930
			   =510
			
			Die Bereiche sollten im Array freqencyBands[] gespeichert werden
		 */

        int count = 0;

		for (int i = 0; i < 8; i++)
		{
			float average = 0;
			int sampleCount = (int)Mathf.Pow(2, i) * 2;

			if (i==7) {
				 sampleCount += 2;  // die zwei fehlenden Samples mit aufnehmen
			}

			for (int j = 0; j < sampleCount; j++)
			{
				average += samples[count] * (count + 1);
				count++;
			}
			average /= count;

			freqencyBands[i] = average * 10;
		}
    }
}
