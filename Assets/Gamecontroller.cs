﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamecontroller : MonoBehaviour {
	// TODO #5

	public float[] freqPlanes = new float[4];  // Frequenzbereiche weiter zusammen
    public float trigger;   // Wert ab wann das Event für das Spiel relevant ist, ggf. ausprobieren - je nach Musik
    GameObject[] gamePlanes = new GameObject[4];  // beinhaltet nach InitiateGamePlanes() die 4 Platten
    public float[] planePoints = new float[4];  // zwischenspeichern der noch zu vergebenen Punkte
	public float[] planeDecrease = new float[4];  // Zwischenspeichern für das "schrumpfen"
	public int totalPoints = 0;  // Gesamtpunktzahl

	// Use this for initialization
	void Start () {
		InitiateGamePlanes();
	}
	
	// Update is called once per frame
	void Update () {
		SummarizeFreqBands();
		ColorPlans();
		CheckUserInput();
        DecreasePlane();
	}

	void InitiateGamePlanes() {
		// Speichern der 4 Gameobjects in einem Array
		for (int i = 0; i < 4; i++)
		{
            gamePlanes[i] = GameObject.Find("/GameController/Plane" + i);
            gamePlanes[i].GetComponent<Renderer>().enabled = false;
            gamePlanes[i].transform.localScale = new Vector3(0, 0, 0);
		}
	}	

	void SummarizeFreqBands() {
		// Erneutes zusammenfassen der Bereiche
		freqPlanes[0] = (AudioPeer.freqencyBands[0] + AudioPeer.freqencyBands[1]) / 2;
        freqPlanes[1] = (AudioPeer.freqencyBands[2] + AudioPeer.freqencyBands[3]) / 2;
        freqPlanes[2] = (AudioPeer.freqencyBands[4] + AudioPeer.freqencyBands[5]) / 2;
        freqPlanes[3] = (AudioPeer.freqencyBands[6] + AudioPeer.freqencyBands[7]) / 2;
	}

	void ColorPlans() {
		// Einfärben der Platten auf initialen zustand
		for (int i = 0; i < 4; i++)
		{
			if(freqPlanes[i] > trigger) {
                gamePlanes[i].GetComponent<Renderer>().enabled = true;
				gamePlanes[i].GetComponent<Renderer>().material.color = new Color(255, 255, 255);
				gamePlanes[i].transform.localScale = new Vector3(1,1,1);
				planePoints[i] = 1f;
            }
		}
	}

	void DecreasePlane() {
		// hier werden die Platten kleiner, wenn keine Benutzeraktion stattgefunden hat
        for (int i = 0; i < 4; i++) {
			if(gamePlanes[i].transform.localScale.x <= 0f) {
				gamePlanes[i].GetComponent<Renderer>().enabled = false;
			} else {
                gamePlanes[i].GetComponent<Renderer>().enabled = true;
                if (freqPlanes[i] > planePoints[i])
                {
                    planePoints[i] = freqPlanes[i];
                    planeDecrease[i] = 0.005f;
                }
                if (freqPlanes[i] < planePoints[i])
                {
                    planePoints[i] -= 0.005f;
                    planeDecrease[i] *= 1.2f;
                    gamePlanes[i].transform.localScale = new Vector3(
                        gamePlanes[i].transform.localScale.x - planeDecrease[i],
                        gamePlanes[i].transform.localScale.y - planeDecrease[i],
                        gamePlanes[i].transform.localScale.z - planeDecrease[i]);
                }
			}
		}
	}

	void CheckUserInput() {
		// Benutzereingabe prüfen und weiterleiten (ich hab das drin gelassen für mein Beispiel)
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			//Debug.Log("Platte 1");
			calcPoints(0);
		}
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //Debug.Log("Platte 2");
            calcPoints(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            //Debug.Log("Platte 3");
            calcPoints(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            //Debug.Log("Platte 4");
            calcPoints(3);
        }
	}

	void calcPoints(int planeNumber) {
		// Punkte addieren oder subtraieren und Fahler bzw. richige Aktion kennzeichen (Farbe/Größe)
		if(gamePlanes[planeNumber].GetComponent<Renderer>().enabled) {
			int pointsToAdd = (int) (gamePlanes[planeNumber].transform.localScale.x * 100);
            gamePlanes[planeNumber].GetComponent<Renderer>().material.color = new Color(0, 255, 0);
			totalPoints += pointsToAdd;
			Debug.Log(pointsToAdd);
		} else {
            gamePlanes[planeNumber].GetComponent<Renderer>().enabled = true;
            gamePlanes[planeNumber].GetComponent<Renderer>().material.color = new Color(255, 0, 0);  // Fehler anzeigen
            gamePlanes[planeNumber].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            totalPoints -= 50;
			Debug.Log(-50);
		}
	}

}
